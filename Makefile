include fifo.mk

ui:
	LEIN_ROOT=1 lein clean
	LEIN_ROOT=1 lein less once
	LEIN_ROOT=1 lein with-profile rel cljsbuild once

generic/rel: ui

version_header:
