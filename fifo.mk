REBAR_VSN = $(shell erl -noshell -eval '{ok, F} = file:consult("rebar.config"), [{release, {_, Vsn}, _}] = [O || {relx, [O | _]} <- F], io:format("~s", [Vsn]), init:stop().')
VARS_VSN = $(shell grep 'bugsnag_app_version' rel/vars.config | sed -e 's/.*,//' -e 's/[^0-9.p]//g' -e 's/[.]$$//')
APP_VSN = $(shell grep vsn apps/$(APP)/src/$(APP).app.src | sed 's/[^0-9.p]//g')

include config.mk


###
### Packaging
###

uname_S := $(shell sh -c 'uname -s 2>/dev/null || echo not')
uname_V6 := $(shell sh -c 'uname -v 2>/dev/null | cut -c-6 || echo not')

ifeq ($(uname_S),Darwin)
	PLATFORM = darwin
endif
ifeq ($(uname_S),FreeBSD)
	PLATFORM = freebsd
endif
ifeq ($(uname_V6),joyent)
	PLATFORM = smartos
endif

dist: ${PLATFORM} ;

generic/rel: ui

freebsd: ${PLATFORM}/rel
	$(MAKE) -C rel/pkgng package

smartos:  ${PLATFORM}/rel
	$(MAKE) -C rel/pkg package

darwin:  ${PLATFORM}/rel

freebsd/rel: generic/rel

smartos/rel: generic/rel

darwin/rel: generic/rel
